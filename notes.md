<!-- * 2.1 Вам потрібно додати нове домашнє завдання в новий репозиторій на Gitlab. -->

Перша дія це клонувати репозиторій з Gilab в потрібну для нас папку з допомогою комади git clone та https посилання на репозиторій, тобто виглядає так git clone https://gitlab.com/xanxhex/repo-3.git

Виконуємо необхідне домашнє завднання в папці репозиторію та в Gitbash перевіряємо зміну з допомогою команди git status(за бажанням). Потім прописуємо git add та назву зміненого файлу або "git add ." шоб додати всі змінені файли.
Прописує "git commit -m "назва коміту"" щоб зафіксувати зміни
І остання дія це "git push" шоб відправити зміни у хмарний репозиторій

 <!--? 2.2 Вам потрібно додати існуюче домашнє завдання в новий репозиторій на Gitlab. -->

Заходимо в папку, де зберігається наш проект. Відкривши термінал в цій папці прописуємо "git innit"
Даллі прописуємо "git remote add origin" і ссилку на репозиторій тобто так
"git remote add origin https://gitlab.com/xanxhex/repo-3.git"
А потім повторюємо вже знайомі дії, пишемо "git add ." потім "git commit -m "назва коміту"", а потім "git push"
